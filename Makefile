CXX=g++
CXXFLAGS=-Wall -std=c++1z -O2
LDFLAGS=-s -lgmp
RM=rm -rf

.PHONY: all clean check

all: asciiprime rmcheck

clean:
	$(RM) asciiprime rmcheck
	
check: all
	./asciiprime TT1989.txt
	./rmcheck result-TT1989.txt 40
	./asciiprime C1972.txt
	./rmcheck result-C1972.txt 40
	
asciiprime: gen.cpp
	$(CXX) $(CXXFLAGS) gen.cpp -o asciiprime $(LDFLAGS)
	
rmcheck: chk.cpp
	$(CXX) $(CXXFLAGS) chk.cpp -o rmcheck $(LDFLAGS)
