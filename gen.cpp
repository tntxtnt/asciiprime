#include <iostream>
#include <string>
#include <cstring>
#include <algorithm>
#include <ctime>
#include <random>
#include <sstream>
#include <fstream>
#include <cctype>
#include <cmath>
#include <boost/multiprecision/gmp.hpp>
#include <boost/multiprecision/miller_rabin.hpp>
using BigInt = boost::multiprecision::mpz_int;

struct AsciiContent {
    std::string data = "";
    int width = 0;
    int height = 0;
    int asteriskIndex = -1;
};

void pprint(std::ostream& out, const BigInt& n, int width);
AsciiContent readAsciiContent(const char* filename);

int main(int argc, char** argv)
{
    if (argc != 2)
    {
        std::cerr << "Usage: asciiprime <ascii file name>\n"
                  << "Example usage: asciiprime C1972.txt\n";
        return 1;
    }
    AsciiContent ascii = readAsciiContent(argv[1]);
    int digits = ascii.width * ascii.height;
    std::string incrStr(digits - ascii.asteriskIndex, '0');
    incrStr[0] = '1';
    BigInt incrAmount{incrStr};
    BigInt n{ascii.data};
    std::mt19937 prng;
    prng.seed(clock());
    int iterCount = digits * log(10) * 10; //10 times average prime gap
    for (int i = 0; i < iterCount; ++i, n += incrAmount)
    {
        if (boost::multiprecision::miller_rabin_test(n, 25, prng))
        {
            pprint(std::cout, n, ascii.width);
            std::ofstream fout(std::string("result-") + argv[1]);
            if (!fout)
            {
                std::cerr << "Cannot open output file `result-" << argv[1] << "`\n";
                exit(1);
            }
            pprint(fout, n, ascii.width);
            break;
        }
    }
    std::cout << "\n";
}

void pprint(std::ostream& out, const BigInt& n, int width)
{
    std::ostringstream oss;
    oss << n;
    int w = 0;
    for (char c : oss.str())
    {
        out << c;
        if (++w == width)
        {
            out << "\n";
            w = 0;
        }
    }
}

AsciiContent readAsciiContent(const char* filename)
{
    std::ifstream fin(filename);
    if (!fin)
    {
        std::cerr << "Cannot open file " << filename << "\n";
        exit(1);
    }
    AsciiContent ret;
    std::string line;
    while (std::getline(fin, line))
    {
        if (ret.width == 0) ret.width = line.size();
        if ((int)line.size() != ret.width)
        {
            std::cerr << "Data has width " << ret.width
                      << " but line #" << ret.height + 1 << " has length "
                      << line.size() << "\n";
            exit(1);
        }
        if (!std::all_of(begin(line), end(line), [](char c){ return isdigit(c); }))
        {
            if (std::count(begin(line), end(line), '*') != 1 || ret.asteriskIndex >= 0)
            {
                std::cerr << "Invalid character or having more than 1 asterisk "
                              "at line #" << ret.height + 1 << ":\n"
                          << line << "\n";
                exit(1);
            }
            ret.asteriskIndex = ret.width * ret.height + line.find('*');
            line[line.find('*')] = '0';
        }
        ret.data += line;
        ret.height++;
    }
    if (ret.asteriskIndex < 0)
    {
        std::cerr << "Input data has 0 asterisks. Please put an asterisk "
                     "somewhere to specify increment spot.\n";
        exit(1);
    }
    return ret;
}