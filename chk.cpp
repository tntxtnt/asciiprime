#include <iostream>
#include <string>
#include <cstring>
#include <algorithm>
#include <ctime>
#include <random>
#include <sstream>
#include <fstream>
#include <cctype>
#include <cstdlib>
#include <boost/multiprecision/gmp.hpp>
#include <boost/multiprecision/miller_rabin.hpp>
using BigInt = boost::multiprecision::mpz_int;

void pprint(const BigInt& n, int width);
BigInt readBigInt(const char* filename);

int main(int argc, char** argv)
{
    if (argc != 3)
    {
        std::cerr << "Usage: rmcheck <big num file name> <number of iterations>\n"
                  << "Example usage: rmcheck C1972-result.txt 40\n";
        return 1;
    }
    for (char* ptr = argv[2]; *ptr; ++ptr) if (!isdigit(*ptr))
    {
        std::cerr << argv[2] << " is not an integer\n";
        return 1;
    }
    
    BigInt n = readBigInt(argv[1]);
    std::mt19937 prng;
    prng.seed(clock());
    if (boost::multiprecision::miller_rabin_test(n, atoi(argv[2]), prng))
    {
        std::cout << "probably prime (good)\n";
    }
    else
    {
        std::cout << "not prime (BAD)\n";
        return 1;
    }
}

BigInt readBigInt(const char* filename)
{
    std::ifstream fin(filename);
    if (!fin)
    {
        std::cerr << "Cannot open file " << filename << "\n";
        exit(1);
    }
    std::string ret, line;
    while (std::getline(fin, line))
    {
        if (std::all_of(begin(line), end(line),
            [](char c){ return isdigit(c); }))
        {
            ret += line;
        }
        else
        {
            std::cerr << "Invalid content\n";
            exit(1);
        }
    }
    return BigInt{ret};
}